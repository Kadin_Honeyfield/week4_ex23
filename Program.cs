﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace week4_ex23
{
    class Program
    {
        static void Main(string[] args)
        {
            //A
            Console.WriteLine("-------Exercise A-------");
            Console.WriteLine();
            Console.WriteLine("(Declare/Create Dictionary)");
            Console.WriteLine();

            var movies = new Dictionary<string, string>();
            movies.Add("Harry Potter","Action");
            movies.Add("Pirates of the Carribean","Action");
            movies.Add("Step Brothers","Comedy");
            movies.Add("Game of Thrones","Action");
            movies.Add("The Avengers","Action");

            //B
            Console.WriteLine("-------Exercise B-------");

            Console.WriteLine("Movies list:");
            
            foreach(var movie in movies)
            {
                if(movies.ContainsValue("Comedy"))
                {
                    Console.WriteLine($"{movie.Key} - {movie.Value}");
                }
            }
            
            

            //C
            Console.WriteLine("-------Exercise C-------");

            int comCount = movies.Count(kpv=>kpv.Value.Contains("Comedy"));
            int actCount = movies.Count(kpv=>kpv.Value.Contains("Action"));
            int draCount = movies.Count(kpv=>kpv.Value.Contains("Drama"));

            Console.WriteLine($"Number of Comedy films = {comCount}");
            Console.WriteLine($"Number of Action films = {actCount}");
            Console.WriteLine($"Number of Drama films = {draCount}");

            //D
            Console.WriteLine("-------Exercise D-------");
            
        }
    }
}
